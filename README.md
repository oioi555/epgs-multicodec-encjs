# EPGS-multiCodec-encjs
[EPGStation](https://github.com/l3tnun/EPGStation)用のエンコードスクリプトです

## 特徴
[EPGStation](https://github.com/l3tnun/EPGStation)標準の[`enc.js`]を、引数にコーデックを指定することで、ソフトウェアエンコード（h.264 h.265）ハードウェアデエンコード（vaapi h264 h265）を使用できるようにしました  
また、過去に[EPGStation](https://github.com/l3tnun/EPGStation)コミュニティで、議論されたエンコードにおける課題についても取り組みました  
動画の冒頭に前番組のストリーム情報が記録されてしまっている場合でも、自動的に切り換わる秒数を検知しエンコードエラーを可能な限り回避します

## 主な機能
+ コーデックの指定
    + [`libx264`], [`libx265`], [`h264_vaapi`], [`hevc_vaapi`]
+ オーディオのエンコード
    + 音声多重放送(デュアルモノ)
        + ffmpeg バージョン4系以降に対応
            + ffmpeg バージョン [`4.2.4`], [`4.4.4`], [`5.1.1`], [`6.0`], [`7.0`] にて動作確認
    +  二重音声放送(デュアルステレオ)
        + `[二]` `[解]` `[多]`の番組で、TSと同じように副音声を残す
+ 字幕付与の自動判定
    +  ffmpegのコンフィグレーションで、[`libaribb24`]が有効でない場合、または字幕放送でない番組は無効にする
+ ハードウェアデコードの自動判定指定（vaapi）
    + CPU負荷を最小限にしたい人向け、不具合がでるTS衛星放送を除外
+  エンコード進捗表示
    + 経過時間を追加
+ 自動削除指定時のTS喪失を回避
     + エンコード失敗時に親プロセスにExitCodeを通知

## 必要条件
+ [EPGStation](https://github.com/l3tnun/EPGStation) >=2.7.x
+ [ffmpeg](https://ffmpeg.org/) (>=4.2.4)
+ ハードウェアエンコード(vaapi) 使用時
    + GPUが利用可能で、[ffmpeg](https://ffmpeg.org/) のコンフィグレーションで[`vaapi`]が有効

## 導入方法
### ファイル配置
本プロジェクトの[`enc.js`]を[`epgstation/config/`]ディレクトリにコピー

### コーデックの指定
無指定の場合は、[`libx264`]  
デフォルトの[`enc.js`]で、利用している方は、ファイルを置き換えるだけでOK  
スクリプト内の、`useCodecDefault`を編集することで、デフォルトのコーデックは変更できます

### 複数のコーデックを選択したい場合
[`epgstation/config/config.yml`]の[`encode:`]を編集

### 例
```
encode:
    - name: H.264(HW)
      cmd: '%NODE% %ROOT%/config/enc.js h264_vaapi'
      suffix: -avcHW.mp4
      rate: 4.0
    - name: H.265(HW)
      cmd: '%NODE% %ROOT%/config/enc.js hevc_vaapi'
      suffix: -nevcHW.mp4
      rate: 4.0
    - name: H.264
      cmd: '%NODE% %ROOT%/config/enc.js libx264'
      suffix: -avc.mp4
      rate: 4.0
    - name: H.265
      cmd: '%NODE% %ROOT%/config/enc.js libx265'
      suffix: -hevc.mp4
      rate: 4.0
```

## VAAPIの導入
作者の環境は、Ubuntu22.04LTS、AMD Ryzen7 5700G（APUの内蔵GPU）  
[l3tnun/docker-mirakurun-epgstation](https://github.com/l3tnun/docker-mirakurun-epgstation)

IntelのCPUの場合、ほぼiGPUが内蔵されていますので、VAAPIは利用可能と思います

### 例（Ryzen APU）
### dockerイメージ
[`docker-mirakurun-epgstation/epgstation/debian.Dockerfile`]を編集  
vaapiドライバー[`mesa-va-drivers`]をインストール
```
RUN apt-get update && \
       ︙
    # for Ryzen APU vaapi
    apt-get -y install mesa-va-drivers && \
\
#ffmpeg build
      ︙
```
[ffmpeg](https://ffmpeg.org/) のコンフィグレーションで[`vaapi`]を有効にする
```
#ffmpeg build
      ︙
      --disable-doc \
      # for Ryzen APU vaapi
      --enable-vaapi \
    && \
    make -j$(nproc) && \
    make install && \
     ︙
```
### docker-compose
[`docker-mirakurun-epgstation/docker-compose.yml`]を編集  
[`devices`]の有効化
```
services:
      ︙
    epgstation:
      ︙
#         user: "1000:1000"
         devices:
             - /dev/dri:/dev/dri
        restart: always
      ︙

```

#### 補足
[`user: "1000:1000"`] を有効にして、EPGStationをユーザー権限で実行している場合は、
デバイスのパーミッションを変更する必要があります  
ターミナルで下記のコマンドを実行
```
echo 'KERNEL=="render*" GROUP="render", MODE="0666"' | sudo tee /etc/udev/rules.d/99-render.rules
sudo udevadm control --reload-rules && sudo udevadm trigger
```
### dockerの再ビルド
[`docker-mirakurun-epgstation/`]パスで、下記のコマンドを実行
```
docker-compose build --no-cache epgstation && docker-compose up -d epgstation
```

## License
[MIT License](LICENSE)
