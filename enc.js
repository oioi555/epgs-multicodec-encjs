// モジュールの読み込み
const {spawn} = require('child_process');
const {execFileSync} = require('child_process');
const {spawnSync} = require('child_process');

// EPGStation設定の読み込み
const epgsConfig = getEpgsConfig();

/**
 * ログについて
 *
 * console.error() で出力されるログは、ログレベルを`debug`にすることで確認できます
 * 設定ファイル: config/serviceLogConfig.yml
 *      categories: encode: level: debug
 * ログファイル: logs/Service/encode.log
 * */

/**
 * ffmpegでエンコードが失敗した場合に限り、ffmpegの詳細ログを出力する指定
 * ログレベルを`debug`にしている場合に有効です
 */
const ffmpegLogOutOnlyOnError = true;

/**
 * 進捗情報の出力制限を指定
 * ログレベルを`debug`にしている場合、進歩情報のログでログが流されてしまうので
 * エンコード中に何回ログを出力するか指定する
 *
 * -1           … 出力制限をしない
 * 0            … 一切出力をしない
 * 5            … 進捗20％ごとに更新
 * 10           … 進捗10％ごとに更新
 * 20           … 進捗 5％ごとに更新
 * 50           … 進捗 2％ごとに更新
 * 100          … 進捗 1％ごとに更新
 */
const progressLogOutMax = 100;

/**
 * エンコードに使用するコーデックを指定
 *
 * libx264      … ソフトウェア h264(avc) mp4
 * libx265      … ソフトウェア h265(hevc) mp4
 * h264_vaapi   … ハードウェア h264(avc) mp4
 * hevc_vaapi   … ハードウェア h265(hevc) mp4
 */
const codecs = ['libx264', 'libx265', 'h264_vaapi', 'hevc_vaapi'];

// デフォルトのコーデック(スクリプト引数が無指定だった場合に使用する)
const useCodecDefault = 'libx264';

// スクリプトの引数からコーデックを指定（codecsの配列に無い場合は、デフォルトを使用）
// useCodecは、ビデオストリームのコーデック指定に利用されます
const useCodec = codecs.includes(process.argv[2])? process.argv[2] : useCodecDefault;

/**
 * vaapi使用時に、変換元動画をハードウェアデコードするか指定
 * ハードウェアデコードを使用すると、CPU及びGPUの負荷を抑えられる
 * ただ、変換速度が遅くなる場合があります
 *
 * no       … すべてソフトウェアデコード（推奨）
 * auto     … TSは地上波のみに使用（TS衛星放送で不具合がでる）
 */
const useHwDecord = 'no';

/**
 * 音声・字幕ストリームに異常があった場合の冒頭カット設定
 * 前番組からの切り替りのタイミングで、正常にストリーム情報が検出できない場合に冒頭をカットします
 * cutSecondDefaultから、cutSecondMaxまで、一秒づつ増やしながら、切り替わりの秒数を求める
 */
const cutSecondDefault = 4; // 最初に試す冒頭カット秒数
const cutSecondMax = 20;    // 検査する最大秒数

/**
 * 音声ストリームの異常が、冒頭カットで改善しない場合、副音声を捨てるか指定
 *
 * true     … 副音声を捨てて、ffmepgのエラーを回避（自動削除指定時は、副音声は失われます）
 * false    … ffmpegのエラーを許容（自動削除指定時は、TSの副音声が残る）
 * */
const cutDiscardAuidoOnError = false;

// 各コーデック固有の設定
(async () => {
    const vaapiDeviceArgs = []; // -i input より前に挿入される引数
    const useCodecArgs = [];    // 指定コーデック固有の引数

    // ソフトウェアエンコード（libx264, libx265）
    if (/libx/.test(useCodec)) {
        // ビデオフィルタ インターレス解除
        useCodecArgs.push('-vf', 'yadif');

        // 品質
        useCodecArgs.push('-preset', 'veryfast', '-crf', '26');
    }

    // ハードウェアエンコード（h264_vaapi, hevc_vaapi）
    if (/_vaapi/.test(useCodec)) {
        // vaapi デバイス指定
        vaapiDeviceArgs.push('-vaapi_device', '/dev/dri/renderD128');

        // useHwDecord = 'auto' 時、HWデコードはTSの衛星放送以外で有効
        if (await isUseHwDecord()) {
            vaapiDeviceArgs.push('-hwaccel', 'vaapi', '-hwaccel_output_format', 'vaapi');
        }

        // ビデオフィルタ
        const videoFilter = [
            'format=nv12|vaapi', 'hwupload',
            'deinterlace_vaapi',        // インターレース解除
            'scale_vaapi=h=720:w=-2'    // ビデオサイズ変換
        ];
        useCodecArgs.push('-vf', videoFilter.join(','));

        // 品質 (1 ~ 51 数値が少ない方が高品質、デフォルト:20)
        useCodecArgs.push('-qp', '20');
    }

    // FFmpegの実行
    spawnFFmepeg(vaapiDeviceArgs, useCodecArgs);
})();

// 字幕の設定を取得
function getSubTitlesArg() {
    const fix = [];
    const map = [];
    let cutSecond = 0;
    const isSub = /\[字\]/.test(getEnv('NAME'));

    if (isTs()) {
        // 字幕ストリーム数に異常があった場合、冒頭カットする必要秒数を検査
        cutSecond = getNeedFirstCutSecond(isSub? 1 : 0, 'Subtitle: arib_caption');
    }

    if (isSub && cutSecond !== -1 && hasFFmpegConfig('libaribb24')) {
        fix.push('-fix_sub_duration');
        map.push('-map', '0:s?', '-c:s', 'mov_text');
    }

    return {fix: fix, map: map, cutSecond: cutSecond};
}

/**
 * 音声の設定を取得
 *
 * 参考にした議論
 * https://github.com/l3tnun/EPGStation/issues/244
 * https://github.com/l3tnun/EPGStation/issues/247
 * https://github.com/l3tnun/EPGStation/issues/388
 * https://github.com/l3tnun/EPGStation/issues/583
 * */
function getAudioArgs(cutSecondSub) {
    const isDualMono = parseInt(getEnv('AUDIOCOMPONENTTYPE'), 10) == 2;
    const args = [];
    let cutSecond = cutSecondSub? cutSecondSub : 0;  // 冒頭カット秒数、先に字幕ストリームで検出していれば引継ぐ
    let audioCount = getFFmpegSsStreams(cutSecond, 'Audio: aac').count;    // 音声ストリーム数

    // 二か国語放送
    const isBilingual = getEnv('NAME').indexOf('[二]') !== -1;

    if (isTs()) {
        // 音声ストリーム数に異常があった場合、冒頭カットする必要秒数を検査
        const needAudioCount = (/\[二\]|\[解\]|\[多\]/.test(getEnv('NAME')) && !isDualMono)? 2 : 1;
        cutSecond = getNeedFirstCutSecond(needAudioCount, 'Audio: aac');
        if (cutSecond)  {
            // 冒頭カットで改善する
            audioCount = needAudioCount;
        } else if (cutSecond === -1) {
            // 冒頭カットしても改善しない
            if ((needAudioCount === 2 && cutDiscardAuidoOnError) || // 副音声を捨てる指定時
                needAudioCount === 1) {  // 副音声は無いはずなので、エラー回避で主音声のみに強制
                audioCount = 1;
            }
        }

        // 音声多重放送(デュアルモノ)
        if (isDualMono) {
            const lang = isBilingual? 'eng' : 'jpn';
            args.push('-filter_complex', '[0:a:0]channelsplit=channel_layout=stereo');
            args.push('-metadata:s:a:0', 'language=jpn');
            args.push('-metadata:s:a:1', 'language=' + lang);
            args.push('-c:a', 'ac3');   // 注意: aac copy ではエラーになる
            return {args: args, cutSecond: cutSecond};
        }
    }

    // 音声マッピング
    for (let index = 0; index < audioCount; index++) {
        args.push('-map', '0:a:' + index);
        if (audioCount > 1) {
            const lang = (isBilingual && index === 1)? 'eng' : 'jpn';
            args.push('-metadata:s:a:' + index, 'language=' + lang);
        }
    }

    // 音声エンコード
    if (audioCount > 1 || cutSecond !== 0 || cutSecondSub !== 0) {
        args.push('-c:a', 'aac');   // 複数音声又は、冒頭をカットする場合は再エンコードする
    } else {
        args.push('-c:a', 'copy');  // 速度優先で、エンコードを省略
    }

    return {args: args, cutSecond: cutSecond};
}

// FFmpegコマンドを、ストリームで実行
async function spawnFFmepeg(vaapiDeviceArgs, useCodecArgs) {

    const sub = getSubTitlesArg();              // 字幕
    const audio = getAudioArgs(sub.cutSecond);  // オーディオ

    // 冒頭カット(ストリーム情報に異常があった場合に挿入)
    const cutSecond = Math.max(audio.cutSecond, sub.cutSecond);
    const ss = (cutSecond)? ['-ss', cutSecond] : [];

    // 引数全体の組み立て
    const outputArgs = [].concat(
        '-y', getAnalyze(), vaapiDeviceArgs, sub.fix, ss, ['-i', getEnv('INPUT')],
        ['-map', '0:v', '-c:v', useCodec], audio.args, useCodecArgs, sub.map, getEnv('OUTPUT')
    );

    // コマンド実行前の準備
    console.error(callerName(), 'ffmpeg ', outputArgs.join(' '));   // 最終の実行コマンド確認
    const duration = getFFprobe('-show_format').format.duration;    // 進捗計算に利用する動画の長さを取得
    const startTime = process.uptime(); // 経過時間計測
    const logBuffers = [];              // 進捗以外のログをまとめる
    let progressLogOutCount = 0;        // 進捗情報の出力制限

    // ffmpegコマンドの実行
    const child = spawn(getEnv('FFMPEG'), outputArgs);

    // ffmpegログの取得
    child.stderr.on('data', data => {
        // エンコード進捗表示処理
        if (String(data).startsWith('frame=')) {
            // ログの余分な空白を排除して連想配列に変換
            const logs = {};
            for (let str of String(data).split('\r')[0].replace(/\s{2,}/g, '').replace(/=\s/g, '=').split(' ')) {
                const keyValue = str.split('=');
                logs[keyValue[0]] = keyValue[1];
            }
            // 経過時間を追加
            logs['elapsed'] = convertSecToTime(process.uptime() -startTime);

            // 進歩に表示する要素と並び順を指定
            // 選択候補 frame fps q size time bitrate dup drop speed elapsed
            const viewKeys = ['frame', 'fps', 'bitrate', 'drop', 'speed', 'elapsed'];
            const viewLogs = [];
            for (let key of viewKeys) {
                if (logs[key] !== undefined) viewLogs.push(key + '=' + logs[key]);
            }

            // 進捗情報の出力制限（標準出力に進捗情報を吐き出す）
            if (progressLogOutMax !== 0) {
                const percent = convertTimeToSec(logs.time) / duration;
                if (percent >= parseFloat(1 / progressLogOutMax) * progressLogOutCount || // 制限あり
                    progressLogOutMax === -1) { // 制限なし
                    progressLogOutCount++;
                    console.log(JSON.stringify({ type: 'progress', percent: percent, log: viewLogs.join(' ')}));
                }
            }
        } else {
            logBuffers.push(String(data).trim());    // 進捗以外のffmpegのログを保存
        }
    });

    // エンコード終了時の処理
    child.on('exit', code => {
        const isError = (code !== 0);   // エラーの判定

        // ffmpegのログを出力
        if (!ffmpegLogOutOnlyOnError || (ffmpegLogOutOnlyOnError && isError)) {
            console.error('spawnFFmepeg: ffmpeg messages:', logBuffers.join('\n'));
        }

        // 経過時間と平均倍率などの結果とエンコード条件を確認
        const elapsed = parseFloat(process.uptime() -startTime);
        const logs = {
            outputArgs: outputArgs.join(' '),
            duration: convertSecToTime(duration),
            elapsedTime: convertSecToTime(elapsed),
            averageSpeed: Math.floor(duration / elapsed) + 'x',
            useCodec, useHwDecord, cutSecond
        };
        const message = isError? 'Error code:' + code : 'Successfully encod:';
        console.error('spawnFFmepeg: ', message, logs);

        // エラーの場合は親プロセスに通知（エンコード失敗時の自動削除を回避）
        // https://github.com/l3tnun/EPGStation/issues/583
        if (isError) throw new Error(code);
    });

    child.on('error', error => {
        console.error('spawnFFmepeg: ', {error});
        throw new Error(err);
    });

    process.on('SIGINT', () => {
        child.kill('SIGINT');
    });
}

/*-----------------------------------------------------
 * 補助関数
 * ----------------------------------------------------*/

// 環境変数を取得
// 実行時に渡される環境変数の内容について
// https://github.com/l3tnun/EPGStation/blob/master/doc/conf-manual.md#エンコード設定
function getEnv(variableName) {
    return process.env[variableName];
}

// 環境変数の確認（Debug用）
function showEnvList() {
    console.error(callerName(), '\n', process.env);
}

// 入力がTSか確認
function isTs() {
    const reg = new RegExp(epgsConfig.recordedFileExtension + '$');
    return (getEnv('INPUT').match(reg) !== null)
}

// 動画開始から任意の秒数経過時の特定ストリームを取得
function getFFmpegSsStreams(second, streamName) {
    const options = [].concat(getAnalyze(), '-ss', second, '-i', getEnv('INPUT'));
    try {
        let child = spawnSync(getEnv('FFMPEG'), options);
        const reg = new RegExp('^  Stream #0:.*' + streamName, 'mg');
        const matches = String(child.stderr).match(reg);
        return {matches: matches, count: (matches === null)? 0 : matches.length};
    } catch (error) { console.error(callerName(), {error}); }
}

// 特定ストリームが、必要なストリーム数に変わる冒頭からの経過秒数を取得
function getNeedFirstCutSecond(needCount, streamName) {
    let cutSecond = 0;
    let streams = getFFmpegSsStreams(0, streamName);
    if (streams.count !== needCount) {
        for (let second = cutSecondDefault; second < cutSecondMax; second++) {
            streams = getFFmpegSsStreams(second, streamName);
            if (streams.count === needCount) { cutSecond = second; break; }
        }
        cutSecond = (cutSecond === 0)? -1 : cutSecond; // 変化がなければタイムアウト
        const message = (cutSecond === -1)? 'Timeout: ' : `Success: `;
        console.error(callerName(), message, {streamName, cutSecond, needCount}, streams.matches);
    }
    return cutSecond;
}

// 動画の解析時間を設定
// 無指定の場合、5秒(5M)
function getAnalyze() {
    return ['-analyzeduration', '10M'];
}

// ffprobeの、showオプションで情報を取得
function getFFprobe(showOptions) {
    const options = [].concat(getAnalyze(), '-v', '0', showOptions, '-of', 'json',  getEnv('INPUT'));
    try {
        const stdout = execFileSync(getEnv('FFPROBE'), options);
        return JSON.parse(stdout);
    } catch (error) { console.error(callerName(), {error}); }
}

// ffmpegで特定のビルドオプションが有効か確認
function hasFFmpegConfig(conf) {
    const options = [].concat(getAnalyze(), '-v', '0', '-buildconf');
    try {
        const stdout = execFileSync(getEnv('FFPROBE'), options);
        return String(stdout).indexOf('--enable-' + conf) !== -1;
    } catch (error) { console.error(callerName(), {error}); }
}

// EPGStationの設定を取得
function getEpgsConfig() {
    const {load} = require('js-yaml');
    const {readFileSync} = require('fs');
    try { return load(readFileSync('/app/config/config.yml', 'utf8'));
    } catch (error) { console.error(callerName(), {error}); }
}

// MirakurunのAPIから情報を取得
async function getMirakurunApi(api) {
    try {
        const response = await fetch(epgsConfig.mirakurunPath + api);
        return await response.json();
    } catch (error) { console.error(callerName(), {error}); }
}

// HWデコード可能かの判定（TSの衛生放送は除外）
async function isUseHwDecord() {
    if (useHwDecord !== 'auto') return false;
    if (isTs()) {
        const ch = await getMirakurunApi('api/services/' + getEnv('CHANNELID'));
        return ch.channel.type === 'GR';
    }
    return true;
}

// 時間表記を秒に変換　(00:00:00 →　second)
function convertTimeToSec(time) {
    const times = time.split(':');
    return parseFloat(times[0]) * 3600 + parseFloat(times[1]) * 60 + parseFloat(times[2]);
}

// 秒を時間表記に変換 (second →　00:00:00)
function convertSecToTime(second) {
    const date = new Date(0);
    date.setSeconds(second);
    return date.toISOString().substring(11, 19);
}

// 関数の名前を返す（Debug用）
function callerName() {
    return callerName.caller.name + ': ';
}
